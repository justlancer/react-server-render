import Link from 'next/link'
import { Button } from 'antd'
import './index.css'


export default ({ children }) => (
    <>
        <header className='pageHeader'>
            公共header
        </header>
        <section className="container">
            {children}
        </section>
    </>
)