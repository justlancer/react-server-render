import App, { Container } from 'next/app'
import React from 'react'
import Layout from '../components/Layout'
import { Provider } from 'react-redux'
import initializeStore from '../store/store'
import withRedux from './../lib/with-redux-app'
import 'antd/dist/antd.css'


class MyApp extends App {
  // App组件的getInitialProps比较特殊
  // 能拿到一些额外的参数
  // Component: 被包裹的组件
  static async getInitialProps(ctx) {
    const { Component } = ctx
    let pageProps = {}
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }
    return {
      pageProps,
    }
  }

  render() {
    const { Component, pageProps } = this.props
    return (
        <Provider store={initializeStore()}>
            <Layout>
                <Component {...pageProps} />
            </Layout>
        </Provider>
    )
  }
}
export default withRedux(MyApp)