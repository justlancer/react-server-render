import Head from 'next/head'
import { Button } from 'antd'

 function Home() {
  return (
    <div className="container">
        <Head>
            <title>用户中心</title>
        </Head>
        <img src='/static/avatar.png' className='img' alt='默认头像' />
        <div className='btnWrap'>
            个人中心
        </div>
        <style jsx>{`
            .btnWrap{
                padding:10px;
                font-size:20px;
                color:#f00;
            }
            .img{
                width:100px;
            }
        `}</style>
    </div>
  )
}
export default Home
