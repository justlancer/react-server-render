import Router from 'next/router'
import { Button } from 'antd'
import Head from 'next/head'
import { connect } from 'react-redux'


// const Comp = dynamic(import('../components/Comp'))   //异步加载组件，只有进入当前页面时这个组件才会被下载

function Home(props) {
    console.log(props)
  return (
    <div className="container">
        <Head>
            <title>SEO首页</title>
        </Head>
        <div className='btnWrap'>
            <Button type='primary' onClick={e=>Router.push(`/user`)} >{props.name}的个人中心</Button>
        </div>
        <div style={{fontSize:20}}>redux数据为{props.count}</div>
        <Button onClick={props.add}>点击派发++事件</Button>

        {/* 异步加载组件
        <Comp /> */}

        <style jsx>{`
            .btnWrap{
                padding:10px;
            }
        `}</style>
    </div>
  )
}

function mapStateToProps(state) {
    const { count } = state
    return {
        count,
    }
}
  
function mapDispatchToProps(dispatch) {
    return {
        add() {
            dispatch({ type: 'add' })
        },
    }
}

Home.getInitialProps=async (data)=>{
    // const moment = await import('moment')    //异步加载模块，只有进入当前页面时这个组件才会被下载
    const result=await new Promise(resolve=>{
        setTimeout(()=>{
            resolve({name:'xiaobai'})
        },100)
    })
    return result
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home)